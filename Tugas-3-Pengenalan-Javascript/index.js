// Tugas 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// Jawaban 
console.log("Jawaban Soal No 1");
var sentence = pertama.substring(0,5)+pertama.substring(12,19)+kedua.substring(0,8)+kedua.toUpperCase().substring(8,18); 
console.log(sentence);

// Tugas 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// Jawaban 
console.log("\nJawaban Soal No 2");
var calculate  = parseInt(kataKedua)* parseInt(kataKetiga) + (parseInt(kataPertama) + parseInt(kataKeempat));
console.log(calculate);

// Tugas 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 
// Jawaban 
console.log("\nJawaban Soal No 3");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);