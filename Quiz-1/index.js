 //Soal 1

function next_date(tanggal , bulan , tahun ){ 
    var date = ""
    if (bulan<=12 && bulan >=1){
        if(tanggal == 29){
            if(bulan == 2 && tahun%4 == 0){
                tanggal=1
                bulan+=1
                tahun+=0 
            }else if(bulan == 2 && tahun%4 != 0){
                tanggal= "Tidak Valid"
                bulan =""
                tahun ="" 
            }else{
                tanggal+=1
                bulan+=0
                tahun+=0 
            }    
        }else if(tanggal == 28){
            if(bulan == 2 && tahun%4==0){
                tanggal+=1
                bulan+=0
                tahun+=0
            
            }else if(bulan == 2 && tahun%4!=0){
                tanggal=1
                bulan+=1
                tahun+=0
            }
            else {
                tanggal+=1
                bulan+=0
                tahun+=0
            }
        }else if(tanggal == 30){
            if(bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11){
                tanggal=1
                bulan+=1
                tahun+=0
            }else{
                tanggal+=1
                bulan+=0
                tahun+=0
            }
        }else if(tanggal == 31){
            if(bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10){
                tanggal=1
                bulan+=1
                tahun+=0
            }else if (bulan==12){
                tanggal=1
                bulan=1
                tahun+=1
            }
        }
        else{
            tanggal+=1
            bulan+=0
            tahun+=0
        }
        //mengubah int ke string
        bulan  = String(bulan)
        if(bulan=="1"){
            bulan=" Januari "
        }else if(bulan=="2"){
            bulan=" Februari "
        }
        else if(bulan=="3"){
            bulan=" Maret "
        }
        else if(bulan=="4"){
            bulan=" April "
        }
        else if(bulan=="5"){
            bulan=" Mei "
        }
        else if(bulan=="6"){
            bulan=" Juni "
        }
        else if(bulan=="7"){
            bulan=" Juli "
        }
        else if(bulan=="8"){
            bulan=" Agustus "
        }
        else if(bulan=="9"){
            bulan=" September "
        }
        else if(bulan=="10"){
            bulan=" Oktober "
        }
        else if(bulan=="11"){
            bulan=" November "
        }
        else if(bulan=="12"){
            bulan=" Desember "
        }
        date = tanggal + bulan + tahun
    }else{
        date = "Tidak Valid"
    }
    return date
} // output : 1 Maret 2020

var tanggal = 31
var bulan = 12
var tahun = 2020
var result = next_date(tanggal,bulan,tahun)

console.log(result)

//Soal 2
function jumlah_kata(nama){
    nama = nama.trim()
    nama = nama.split(" ")
    return console.log(nama.length)
}
var kalimat_1 =  " Halo nama saya Muhammad Iqbal Mubarok  "
var kalimat_2 = "Saya Iqbal"

console.log("Soal 2")
jumlah_kata(kalimat_1)
jumlah_kata(kalimat_2)



