//soal 1 
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sortDaftar = daftarHewan.sort()
var length = sortDaftar.length

console.log("Jawaban Soal No 1")
for(var i = 0;i<length;i++){
    console.log(sortDaftar[i])
}

//Soal 2
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

function introduce(data){
    let name = data.name 
    let age = data.age
    let address = data.address
    let hobby = data.hobby

    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`
}

var perkenalan = introduce(data)

console.log("\nJawaban Soal No 2")
console.log(perkenalan)

//Soal 3
function hitung_huruf_vokal(input){
    var inputLowerCase = input.toLowerCase()
    var inputSplit = inputLowerCase.split("")
    var hitung = 0
    for(i=0;i<inputSplit.length;i++){
        if(inputSplit[i] == "a"||inputSplit[i] == "i"||inputSplit[i] == "u"||inputSplit[i]=="e"||inputSplit[i] == "o"){
                hitung++
        }
        
    }
    
    return hitung
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log("\nJawaban Soal No 3")
console.log(hitung_1 , hitung_2)


//soal 4 
function hitung(angka){
    let rumus = 2 * angka - 2
    
    return rumus
}

console.log("\nJawaban Soal No 4")
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8



