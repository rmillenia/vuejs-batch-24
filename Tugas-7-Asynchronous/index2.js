var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
let i = 0;
function execute(time){
 readBooksPromise(time,books[i]).then(function(sisaWaktu){
    i++;
    if (sisaWaktu !== 0 && i < books.length){
      	execute(sisaWaktu)
    }
  }).catch(function(err){
    console.log(err)
  })
}

console.log("Jawaban Soal No. 2")
execute(10000)
