//Tugas 1
var nilai = 77;
console.log("Jawaban Soal No 1");
if ( nilai >= 85 ) {
    console.log("Nilai : "+nilai+"  \nIndeks : A")
} else if(nilai >= 75 && nilai < 85) {
    console.log("Nilai : "+nilai+"  \nIndeks : B")
} else if (nilai >=65 && nilai < 75){
    console.log("Nilai : "+nilai+"  \nIndeks : C")
}
else if (nilai >=55 && nilai < 65){
    console.log("Nilai : "+nilai+" \nIndeks : D")
}
else if (nilai < 55){
    console.log("Nilai : "+nilai+" \nIndeks : E")
}

//Tugas 2
var tanggal =16;
var bulan = 5;
var tahun = 2000;
console.log("\nJawaban Soal No 2");
switch(bulan) {
    case 1:   { console.log(tanggal + ' Januari '+tahun); break;}
    case 2:   { console.log(tanggal + ' Februari '+tahun);break;}
    case 3:   { console.log(tanggal + ' Maret '+tahun);break;}
    case 4:   { console.log(tanggal + ' April '+tahun);break;}
    case 5:   { console.log(tanggal + ' Mei '+tahun);break;}
    case 6:   { console.log(tanggal + ' Juni '+tahun);break;}
    case 7:   { console.log(tanggal + ' Juli'+tahun);break;}
    case 8:   { console.log(tanggal + ' Agustus '+tahun);break;}
    case 9:   { console.log(tanggal + ' September '+tahun);break;}
    case 10:  { console.log(tanggal + ' Oktober '+tahun);break;}
    case 11:  { console.log(tanggal + ' November '+tahun);break;}
    case 12:  { console.log(tanggal + ' Desember '+tahun);break;}
    default:  { console.log('Tidak terjadi apa-apa'); }
}

//Tugas 3
var n = 7;
var output="";
console.log("\nJawaban Soal No 3");
for(var j=1;j<=n;j++){
    for(var k=1;k<=j;k++){
        output+="#"
        
    }
    console.log(output);
    output = "";
}

//Tugas 4
var m = 13;
var jumlah = 1;
var batas ="";
console.log("\nJawaban Soal No 4");
while(jumlah <= m && m >= 1) {
    if(jumlah%3==0) {
        console.log(jumlah+" - I love VueJS")
        for(var n=0;n<jumlah;n++){
            batas += "="           
        }
        console.log(batas)  
    } else if(jumlah%3==1){
        console.log(jumlah+" - I love programming")
    } else if(jumlah%3==2){
        console.log(jumlah+" - I love Javascript")
    }
    jumlah++; 
    batas ="";
}
