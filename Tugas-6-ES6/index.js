  //Soal 1
  const hitung_luas = (p, l) => {
      let luas = p*l
      console.log('Luas Persegi Panjang : '+luas)
  }
  const hitung_keliling = (p, l) => {
    let keliling = 2 * (p + l)   
    console.log('Keliling Persegi Panjang : '+keliling)
  }
   
  const panjang = 4;
  const lebar = 6;
  
  console.log('Jawaban Soal No. 1')
  hitung_luas(panjang, lebar)
  hitung_keliling(panjang, lebar)
   
  //Soal 2
  const newFunction = (firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName:()=>{
        console.log(`${firstName} ${lastName}`)
      }
    }
  }
  
  console.log('\nJawaban Soal No. 2')
  //Driver Code 
  newFunction("William", "Imoh").fullName()
   
  //Soal 3
  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }
  
  const {firstName, lastName, address, hobby} = newObject
    
  console.log('\nJawaban Soal No. 3')
  console.log(firstName, lastName, address, hobby)
   
  //Soal 4
  const west = ["Will", "Chris", "Sam", "Holly"]
  const east = ["Gill", "Brian", "Noel", "Maggie"]
  let combined = [...west,...east]
  
  console.log('\nJawaban Soal No. 4')
  console.log(combined)
   
  //Soal 5
  const planet = "earth" 
  const view = "glass" 
  const sentence = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
   
  console.log('\nJawaban Soal No. 5')
  console.log(sentence)
   
   